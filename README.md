# Foobar

Foobar is a Python library for dealing with word pluralization.

## Installation

```bash
npm i
```

## Usage

### Generating Sprint

Place icons in /src/assets/icons as SVG and run

```bash
npm run generate:svg-sprite
```

In your app.module.ts is a specified path (IconSpriteModule.forRoot({ path: 'assets/sprites/sprite.svg' })).
Change that to your needs if you want to.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
